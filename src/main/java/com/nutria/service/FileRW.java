package com.nutria.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.nutria.model.Companies;
import com.nutria.model.Company;

public class FileRW {

	private static String FILE_NAME = "/files/companySet.txt";

	/**
	 * 
	 * @return Set<String>
	 */
	public static Set<String> getOldCompanies() {
		Set<String> result = null;
		try {
			URL url = FileRW.class.getResource(FILE_NAME);
			FileInputStream inputStream = new FileInputStream(url.getFile());
			String content = IOUtils.toString(inputStream);
			String[] array = content.split(";");
			String[] trimmedArray = new String[array.length];
			for (int i = 0; i < array.length; i++) {
				trimmedArray[i] = array[i].trim();
			}

			result = new TreeSet<String>(Arrays.asList(trimmedArray));
			for (String s : result) {
				s = s.trim();
			}

			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 
	 * @param data
	 * @return {@link File}
	 */
	public static File writeOldCompanies(Companies data) {
		File finalFile = null;
		try {
			URL url = FileRW.class.getResource(FILE_NAME);
			FileInputStream inputStream = new FileInputStream(url.getFile());
			String content = IOUtils.toString(inputStream);
			StringBuilder builder = new StringBuilder(content);
			for (Company s : data.getCompanies()) {
				builder.append(s.getId() + ";");
			}

			finalFile = new File(url.getFile());
			FileUtils.write(finalFile, builder.toString());

		} catch (IOException e) {
			e.printStackTrace();
		}
		return finalFile;
	}
}
