package com.nutria.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.nutria.model.Company;

public class POIService {

	private static HSSFWorkbook workbook;

	/**
	 * 
	 * @param companies
	 * @return {@link HSSFWorkbook}
	 */
	public static HSSFWorkbook writeExcel(List<Company> companies) {
		workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Sample sheet");

		Map<String, String> data = new HashMap<String, String>();

		for (Company c : companies) {
			data.put(c.getId(), c.getName());
		}

		int rownum = 0;
		Row row = sheet.createRow(rownum++);
		Cell cell = row.createCell(0);
		cell.setCellValue("ID");
		cell = row.createCell(1);
		cell.setCellValue("Name");

		for (Map.Entry<String, String> entry : data.entrySet()) {
			row = sheet.createRow(rownum++);
			cell = row.createCell(0);
			String id = entry.getKey();
			cell.setCellValue(id);
			cell = row.createCell(1);
			String name = entry.getValue();
			cell.setCellValue(name);
		}

		return workbook;

	}

}
