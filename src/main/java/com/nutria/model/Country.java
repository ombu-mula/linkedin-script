package com.nutria.model;

public enum Country {

	AUSTRALIA("au", "Australia"),
	BOLIVIA("bo", "Bolivia"),
	BRAZIL("br", "Brazil"),
	CANADA("ca", "Canada"),
	CHILE("cl", "Chile"),
	COLOMBIA("co", "Colombia"),
	COSTA_RICA("cr", "Costa Rica"),
	ECUADOR("ec", "Ecuador"),
	MEXICO("mx", "Mexico"),
	NEW_ZEALAND("nz", "New Zealand"),
	PANAMA("pa", "Panama"),
	PARAGUAY("py", "Paraguay"),
	PERU("pe", "Peru"),
	PORTUGAL("pt", "Portugal"),
	SPAIN("es", "Spain"),
	SWITZERLAND("ch", "Switzerland"),
	UK("gb", "United Kingdom"),
	UNITED_STATES("us", "United States"),
	URUGUAY("uy", "Uruguay"),
	VENEZUELA("ve", "Venezuela");


	private String code;
	private String name;

	private Country(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


}

