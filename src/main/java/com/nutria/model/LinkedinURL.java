package com.nutria.model;

public class LinkedinURL {

	private static String base = "http://api.linkedin.com/v1/company-search:(companies)?";
	private String industry;
	private int count;
	private int start;
	private String company_size;
	private String country;

	public LinkedinURL(String industry, int count, int start,
			String company_size, String country) {
		this.industry = industry;
		this.count = count;
		this.start = start;
		this.company_size = company_size;
		this.country = country;
	}

	@Override
	public String toString() {
		return base + "facet=location," + this.getCountry() + ":0"
				+ "&facet=industry," + this.getIndustry()
				+ "&facet=company-size," + this.getCompany_size() + "&count="
				+ this.getCount() + "&start=" + this.getStart()
				+ "&sort=relevance";
	}

	/**
	 * @return the industry
	 */
	public String getIndustry() {
		return industry;
	}

	/**
	 * @param industry
	 *            the industry to set
	 */
	public void setIndustry(String industry) {
		this.industry = industry;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}

	/**
	 * @param start
	 *            the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}

	/**
	 * @return the company_size
	 */
	public String getCompany_size() {
		return company_size;
	}

	/**
	 * @param company_size
	 *            the company_size to set
	 */
	public void setCompany_size(String company_size) {
		this.company_size = company_size;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

}
