package com.nutria.model;

public class CompanyForm {

	private String type;
	private String start;
	private Companies result;
	private int resultCount;
	private String employeeRange;
	private String country;

	public CompanyForm() {

	}

	public String getType() {
		return type;
	}

	/**
	 * @return the start
	 */
	public String getStart() {
		return start;
	}

	/**
	 * @param start
	 *            the start to set
	 */
	public void setStart(String start) {
		this.start = start;
	}

	/**
	 * @return the result
	 */
	public Companies getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(Companies result) {
		this.result = result;
	}

	/**
	 * @return the resultCount
	 */
	public int getResultCount() {
		return resultCount;
	}

	/**
	 * @param resultCount
	 *            the resultCount to set
	 */
	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}

	/**
	 * @return the employeeRange
	 */
	public String getEmployeeRange() {
		return employeeRange;
	}

	/**
	 * @param employeeRange
	 *            the employeeRange to set
	 */
	public void setEmployeeRange(String employeeRange) {
		this.employeeRange = employeeRange;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

}
