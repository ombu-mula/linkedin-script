package com.nutria.model;


public enum CompanyType {
	
//	Code	Group	Description
	ACCOUNTING(47, "corp fin", "Accounting"),
	AIRLINES_AVIATION(94, "man tech tran", "Airlines/Aviation"),
	ALTERNATIVE_DISPUTE_RESOLUTION(120, "leg org", "Alternative Dispute Resolution"),
	ALTERNATIVE_MEDICINE(125, "hlth" , "Alternative Medicine"),
	ANIMATION(127, "art med" , "Animation"),
	APPAREL_AND_FASHION(19, "good" , "Apparel & Fashion"),
	ARCHITECTURE_AND_PLANNING(50, "cons", "Architecture & Planning"),
	ARTS_AND_CRAFTS(111, "art med rec"	, "Arts and Crafts"),
	AUTOMOTIVE(53, "man", "Automotive"),
	AVIATION_AND_AEROSPACE(52, "gov man", "Aviation & Aerospace"),
	BANKING(41, "fin", "Banking"),
	BIOTECHNOLOGY(12, "gov hlth tech", "Biotechnology"),
	BROADCAST_MEDIA(36, "med rec", "Broadcast Media"),
	BUILDING_MATERIALS(49, "cons" , "Building Materials"),
	BUSINESS_SUPPLIES_AND_EQUIPMENT(138, "corp man"	, "Business Supplies and Equipment"),
	CAPITAL_MARKETS(129, "fin", "Capital Markets"),
	CHEMICALS(54, "man"	, "Chemicals"),
	CIVIC_AND_SOCIAL_ORGANIZATION(90, "org serv", "Civic & Social Organization"),
	CIVIL_ENGINEERING(51, "cons gov", "Civil Engineering"),
	COMMERCIAL_REAL_ESTATE(128, "cons corp fin", "Commercial Real Estate"),
	COMPUTER_AMD_NETWORK_SECURITY(118, "tech", "Computer & Network Security"),
	COMPUTER_GAMES(109, "med rec", "Computer Games"),
	COMPUTER_HARDWARE(3, "tech"	, "Computer Hardware"),
	COMPUTER_NETWORKING(5, "tech", "Computer Networking"),
	COMPUTER_SOFTWARE(4, "tech", "Computer Software"),
	CONSTRUCTION(48, "cons", "Construction"),
	CONSUMER_ELECTRONICS(24, "good man", "Consumer Electronics"),
	CONSUMER_GOODS(25, "good man", "Consumer Goods"),
	CONSUMER_SERVICES(91, "org serv", "Consumer Services"),
	COSMETICS(18, "good", "Cosmetics"),
	DAIRY(65, "agr", "Dairy"),
	DEFENSE_AND_SPACE(1, "gov tech", "Defense & Space"),
	DESIGN(	99, "art med", "Design"),
	EDUCATION_MANAGEMENT(69, "edu", "Education Management"),
	E_LEARNING(132, "edu org", "E-Learning"),
	ELECTRICAL_LECTRONIC_MANUFACTURING(112, "good man", "Electrical/Electronic Manufacturing"),
	ENTERTAINMENT(28, "med rec", "Entertainment"),
	ENVIRONMENTAL_SERVICES(86, "org serv", "Environmental Services"),
	EVENTS_SERVICES(110, "corp rec serv", "Events Services"),
	EXECUTIVE_OFFICE(76, "gov", "Executive Office"),
	FACILITIES_SERVICES(122, "corp serv", "Facilities Services"),
	FARMING(63, "agr", "Farming"),
	FINANCIAL_SERVICES(43, "fin", "Financial Services"),
	FINE_ART(38, "art med rec", "Fine Art"),
	FISHERY(66, "agr", "Fishery"),
	FOOD_AND_BEVERAGES(34, "rec serv", "Food & Beverages"),
	FOOD_PRODUCTION(23, "good man serv", "Food Production"),
	FUND_RAISING(101, "org", "Fund-Raising"),
	FURNITURE(26, "good man", "Furniture"),
	GAMBLING_AND_CASINOS(29, "rec", "Gambling & Casinos"),
	GLASS(145, "cons man", "Glass Ceramics & Concrete"),
	GOVERNMENT_ADMINISTRATION(75, "gov", "Government Administration"),
	GOVERNMENT_RELATIONS(148, "gov", "Government Relations"),
	GRAPHIC_DESIGN(140, "art med", "Graphic Design"),
	HEALTH	(124, "hlth rec", "Health  Wellness and Fitness"),
	HIGHER_EDUCATION(68, "edu", "Higher Education"),
	HOSPITAL_AND_HEALTH_CARE(14, "hlth", "Hospital & Health Care"),
	HOSPITALITY(31, "rec serv tran", "Hospitality"),
	HUMAN_RESOURCES(137, "corp", "Human Resources"),
	IMPORT_AND_EXPORT(134, "corp good tran", "Import and Export"),
	INDIVIDUAL_AND_FAMILY_SERVICES(88, "org serv", "Individual & Family Services"),
	INDUSTRIAL_AUTOMATION(147, "cons man", "Industrial Automation"),
	INFORMATION_SERVICES(84, "med serv", "Information Services"),
	INFORMATION_TECHNOLOGY_AND_SERVICES(96, "tech", "Information Technology and Services"),
	INSURANCE(42, "fin", "Insurance"),
	INTERNATIONAL_AFFAIRS(74, "gov", "International Affairs"),
	INTERNATIONAL_TRADE_AND_DEVELOPMENT(141, "gov org", "International Trade and Development"),
	INTERNET(6, "tech", "Internet"),
	INVESTMENT_BANKING(45, "fin", "Investment Banking"),
	INVESTMENT_MANAGEMENT(46, "fin", "Investment Management"),
	JUDICIARY(73, "gov leg", "Judiciary"),
	LAW_ENFORCEMENT(77, "gov leg", "Law Enforcement"),
	LAW_PRACTICE(9, "leg", "Law Practice"),
	LEGAL_SERVICES(10, "leg", "Legal Services"),
	LEGISLATIVE_OFFICE(72, "gov leg", "Legislative Office"),
	LEISURE(30, "rec serv tran", "Leisure  Travel & Tourism"),
	LIBRARIES(85, "med rec serv", "Libraries"),
	LOGISTICS_AND_SUPPLY_CHAIN(116, "corp tran", "Logistics and Supply Chain"),
	LUXURY_GOODS_AND_JEWELRY(143, "good", "Luxury Goods & Jewelry"),
	MACHINERY(55, "man", "Machinery"),
	MANAGEMENT_CONSULTING(11, "corp", "Management Consulting"),
	MARITIME(95, "tran", "Maritime"),
	MARKET_RESEARCH(97, "corp", "Market Research"),
	MARKETING_AND_ADVERTISING(80, "corp med", "Marketing and Advertising"),
	MECHANICAL_OR_INDUSTRIAL_ENGINEERING(135, "cons gov man", "Mechanical or Industrial Engineering"),
	MEDIA_PRODUCTION(126, "med rec", "Media Production"),
	MEDICAL_DEVICES	(17, "hlth", "Medical Devices"),
	MEDICAL_PRACTICE(13, "hlth", "Medical Practice"),
	MENTAL_HEALTH_CARE(139, "hlth", "Mental Health Care"),
	MILITARY(71, "gov", "Military"),
	MINING_AND_METALS(56, "man", "Mining & Metals"),
	MOTION_PICTURES_AND_FILM(35, "art med rec","Motion Pictures and Film"),
	MUSEUMS_AND_INSTITUTIONS(37, "art med rec", "Museums and Institutions"),
	MUSIC(115, "art rec", "Music"),
	NANOTECHNOLOGY(114, "gov man tech", "Nanotechnology"),
	NEWSPAPERS(81, "med rec", "Newspapers"),
	NON_PROFIT_ORGANIZATION_MANAGEMENT(100, "org", "Non-Profit Organization Management"),
	OIL_AND_ENERGY(57, "man", "Oil & Energy"),
	ONLINE_MEDIA(113, "med"	, "Online Media"),
	OUTSOURCING_OFFSHORING(123, "corp", "Outsourcing/Offshoring"),
	PACKAGE_FREIGHT_DELIVERY(87, "serv tran", "Package/Freight Delivery"),
	PACKAGING_AND_CONTAINERS(146, "good man", "Packaging and Containers"),
	PAPER_AND_FOREST_PRODUCTS(61, "man", "Paper & Forest Products"),
	PERFORMING_ARTS(39, "art med rec", "Performing Arts"),
	PHARMACEUTICALS(15, "hlth tech", "Pharmaceuticals"),
	PHILANTHROPY(131, "org", "Philanthropy"),
	PHOTOGRAPHY(136, "art med rec" , "Photography"),
	PLASTICS(117, "man", "Plastics"),
	POLITICAL_ORGANIZATION(	107, "gov org", "Political Organization"),
	PRIMARY_SECONDARY_EDUCATION(67, "edu", "Primary/Secondary Education"),
	PRINTING(83, "med rec" , "Printing"),
	PROFESSIONAL_TRAINING_AND_COACHING(105, "corp", "Professional Training & Coaching"),
	PROGRAM_DEVELOPMENT(102, "corp org",  "Program Development"),
	PUBLIC_POLICY(79, "gov"	, "Public Policy"),
	PUBLIC_RELATIONS_AND_COMMUNICATIONS(98, "corp", "Public Relations and Communications"),
	PUBLIC_SAFETY(78, "gov"	, "Public Safety"),
	PUBLISHING(82, "med rec", "Publishing"),
	RAILROAD_MANUFACTURE(62, "man", "Railroad Manufacture"),
	RANCHING(64, "agr", "Ranching"),
	REAL_ESTATE(44, "cons fin good", "Real Estate"),
	RECREATIONAL_FACILITIES_AND_SERVICES(40, "rec serv", "Recreational Facilities and Services"),
	RELIGIOUS_INSTITUTIONS(89, "org serv", "Religious Institutions"),
	RENEWABLES_AND_ENVIRONMENT(144, "gov man org"	, "Renewables & Environment"),
	RESEARCH(70, "edu gov", "Research"),
	RESTAURANTS(32, "rec serv", "Restaurants"),
	RETAIL(27, "good man", "Retail"),
	SECURITY_AND_INVESTIGATIONS(121, "corp org serv", "Security and Investigations"),
	SEMICONDUCTORS(7, "tech", "Semiconductors"),
	SHIPBUILDING(58, "man", "Shipbuilding"),
	SPORTING_GOODS(20, "good rec", "Sporting Goods"),
	SPORTS(33, "rec" , "Sports"),
	STAFFING_AND_RECRUITING(104, "corp", "Staffing and Recruiting"),
	SUPERMARKETS(22, "good", "Supermarkets"),
	TELECOMMUNICATIONS(8, "gov tech", "Telecommunications"),
	TEXTILES(60, "man", "Textiles"),
	THINK_TANKS(130	,"gov org", "Think Tanks"),
	TOBACCO(21, "good", "Tobacco"),
	TRANSLATION_AND_LOCALIZATION(108, "corp gov serv", "Translation and Localization"),
	TRANSPORTATION_TRUCKING_RAILROAD(92, "tran", "Transportation/Trucking/Railroad"),
	UTILITIES(59, "man", "Utilities"),
	VENTURE_CAPITAL_AND_PRIVATE_EQUITY(106, "fin tech", "Venture Capital & Private Equity"),
	VETERINARY(16, "hlth", "Veterinary"),
	WAREHOUSING(93, "tran", "Warehousing"),
	WHOLESALE(133, "good" , "Wholesale"),
	WINE_AND_SPIRITS(142, "good man rec" , "Wine and Spirits"),
	WIRELESS(119, "tech" , "Wireless"),
	WRITING_AND_EDITING(103, "art med rec", "Writing and Editing");

	private int code;
	private String group;
	private String description;
	
	
	private CompanyType(int code, String group, String description){
		this.code = code;
		this.group = group;
		this.description = description;
	}


	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public String getGroup() {
		return group;
	}


	public void setGroup(String group) {
		this.group = group;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

}
