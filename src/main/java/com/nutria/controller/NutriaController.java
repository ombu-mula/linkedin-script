package com.nutria.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.stream.StreamSource;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.nutria.model.Companies;
import com.nutria.model.Company;
import com.nutria.model.CompanyForm;
import com.nutria.model.CompanyType;
import com.nutria.model.Country;
import com.nutria.model.LinkedinURL;
import com.nutria.service.FileRW;
import com.nutria.service.POIService;

@Controller
public class NutriaController {

	private String apiKey;
	private String apiSecret;
	private String token;
	private String tokenSecret;
	private LinkedinURL url;
	private static String RESULT = "result";
	private static String ERROR = "error";
	private static String PROP_FILE = "/files/credentials.properties";

	@Autowired
	private Jaxb2Marshaller jaxb2Marshaller;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView init(Model model) {
		this.initialize();
		model.addAttribute("companyTypes", EnumSet.allOf(CompanyType.class));
		model.addAttribute("countries", EnumSet.allOf(Country.class));
		CompanyForm form = new CompanyForm();
		form.setStart("0");
		return new ModelAndView("nutria", "form", form);
	}

	@RequestMapping("/back")
	public String back() {
		return "redirect:/";
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String search(@ModelAttribute("form") CompanyForm form,
			HttpServletRequest req, HttpServletResponse resp, ModelMap model) {
		try {
			req.getSession().removeAttribute(RESULT);
			req.getSession().removeAttribute(ERROR);
			String start = (String) (form.getStart().isEmpty() ? "0" : form
					.getStart());
			if (form.getType() != null) {
				this.url = new LinkedinURL(form.getType(), 20,
						Integer.valueOf(start), form.getEmployeeRange(), form.getCountry());
			}

			Companies result = new Companies();
			result.setCompanies(new ArrayList<Company>());
			result = this.getResponse(req, resp, form.getResultCount(),
					form.getEmployeeRange());

			if (result.getError() != null && !result.getError().isEmpty()) {
				req.getSession().setAttribute(ERROR, result.getError());
			}

			req.getSession().setAttribute(RESULT, result);
			model.addAttribute("result", result);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "nutriaResults";
	}

	@SuppressWarnings("static-access")
	@RequestMapping(value = "/export")
	public String export(@ModelAttribute("form") CompanyForm form,
			HttpServletRequest req, HttpServletResponse resp, ModelMap model) {
		Companies companies = (Companies) req.getSession().getAttribute(RESULT);

		try {
			FileRW write = new FileRW();
			write.writeOldCompanies(companies);
			HSSFWorkbook workbook = POIService.writeExcel(companies
					.getCompanies());

			resp.setContentType("application/vnd.ms-excel");
			resp.setHeader("Content-Disposition",
					"attachment; filename=export.xls");

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			workbook.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();
			resp.setContentLength(outArray.length);

			OutputStream outStream = resp.getOutputStream();
			outStream.write(outArray);
			outStream.flush();
			resp.getOutputStream().close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return "/";

	}

	/**
	 * 
	 */
	public void initialize() {
		Properties prop = new Properties();
		try {
			URL url = FileRW.class.getResource(PROP_FILE);
			prop.load(new FileInputStream(url.getFile()));

			this.apiKey = prop.getProperty("apiKey");
			this.apiSecret = prop.getProperty("apiSecret");
			this.token = prop.getProperty("token");
			this.tokenSecret = prop.getProperty("tokenSecret");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param req
	 * @param resp
	 * @param resultCount
	 * @param companySize
	 * @return {@link Companies}
	 * @throws ServletException
	 * @throws IOException
	 */
	protected Companies getResponse(HttpServletRequest req,
			HttpServletResponse resp, int resultCount, String companySize)
			throws ServletException, IOException {
		String content;
		try {

			OAuthService oAuthService = new ServiceBuilder()
					.provider(LinkedInApi.class).apiKey(apiKey)
					.apiSecret(apiSecret).build();

			Companies result = new Companies();
			result.setCompanies(new ArrayList<Company>());

			Set<String> oldCompanies = FileRW.getOldCompanies();

			int i = 0;
			int count = 0;

			while (count < resultCount) {
				// For out account max results are 20, we have
				// to send multiple request
				url.setStart(url.getStart() + (20 * i));
				OAuthRequest oAuthRequest = new OAuthRequest(Verb.GET,
						url.toString());
				oAuthService.signRequest(new Token(token, tokenSecret),
						oAuthRequest);
				Response response = oAuthRequest.send();
				content = response.getBody();

				// a lo mersa
				if (content.contains("<status>403</status>")) {
					result.setError("Throttle limit for calls to this resource is reached.");
					return result;
				}

				InputStream stream = new ByteArrayInputStream(
						content.getBytes());
				Companies temp = (Companies) jaxb2Marshaller
						.unmarshal(new StreamSource(stream));

				if (temp.getCompanies().size() == 0) {
					return result;
				}

				// Remove old companies stored in file for result
				for (Company company : temp.getCompanies()) {
					if (!oldCompanies.contains(company.getId())) {
						result.getCompanies().add(company);
						count++;
					}
				}
				i++;
			}

			return result;

		} catch (Exception e) {
			content = "ERR";
			e.printStackTrace();
			System.out.println(content);
			return null;
		}
	}

}