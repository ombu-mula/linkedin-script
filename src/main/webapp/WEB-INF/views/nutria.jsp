<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tranqui la nutria</title>
</head>
<body>

	<form:form id="companyForm" action="search" modelAttribute="form"
		method="POST">

		<h2>Search for nutrias</h2>
		<table>
			<tr>
				<td><form:label path="type">Company Type:</form:label></td>
				<td><form:select multiple="false" path="type">
						<form:options items="${companyTypes}" itemValue="code"
							itemLabel="description" />
					</form:select></td>
			</tr>
			<tr>
				<td><form:label path="country">Country:</form:label></td>
				<td><form:select multiple="false" path="country">
						<form:options items="${countries}" itemValue="code"
							itemLabel="name" />
					</form:select></td>
			</tr>
			<tr>
				<td><form:label path="start">Index start:</form:label></td>
				<td><form:input path="start"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="resultCount">Results:</form:label></td>
				<td><select id="resultCount" name="resultCount">
						<option value="100">100</option>
						<option value="200">200</option>
						<option value="300">300</option>
						<option value="400">400</option>
						<option value="500">500</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="employeeRange">Employee Range:</form:label></td>
				<td><select id="employeeRange" name="employeeRange">
						<option value="A">1</option>
						<option value="B">2-10</option>
						<option value="C">11-50</option>
						<option value="D">51-200</option>
						<option value="E">201-500</option>
						<option value="F">501-1000</option>
						<option value="G">1001-5000</option>
						<option value="I">5001-10000</option>
						<option value="I">10000+</option>
				</select></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Search" /></td>
			</tr>
		</table>

	</form:form>
</body>
</html>