<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Results</title>
</head>
<body>

	Error, te queres matar:
	<c:out value="${error}"></c:out>

	<c:if test="${not empty result.companies}">
		<table style="width: 80%" border="1">
			<tr>
				<td><b>#<b></b></td>
				<td><b>ID<b></b></td>
				<td><b>Name</b></td>
			</tr>
			<c:forEach items="${result.companies}" varStatus="status" var="c">
				<tr>
					<td><c:out value="${status.index}" /></td>
					<td><c:out value="${c.id}" /></td>
					<td><c:out value="${c.name}" /></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
	<br>

	<input type="button" title="Back" value="back"
		onclick="location.href='back.html'">

	<c:if test="${empty error}">
		<input type="button" title="Export" value="export"
			onclick="location.href='export.html'">
	</c:if>

</body>
</html>